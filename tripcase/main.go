package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var cfg Config

func init() {
	log.SetFlags(log.Lshortfile | log.Ltime | log.Ldate)

	file, _ := os.Open("cfg.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&cfg)
	if err != nil {
		log.Println("ERROR", err)
	} else {
		log.Println("INIT", "config search read")
	}
}

func main() {
	log.Println("MAIN START")

	http.HandleFunc("/tripcase/", TripcaseHandler)
	http.ListenAndServe(":12345", nil)

	log.Println("MAIN END")
}

func TripcaseHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var req TripcaseRequest
	var res string
	err := decoder.Decode(&req)
	if err != nil {
		res = "{}"
		log.Println("ERROR", "parsing JSON request")
	} else {
		log.Println(req)
		res = Add(&req)
	}

	fmt.Fprintf(w, res)
}

func Add(r *TripcaseRequest) string {
	iir := ImportItineraryRequest{}
	iir.Version = "2.0"
	iir.UserEmail = r.PnrEmail
	iir.PartnerCred.AuthToken = cfg.Token

	result := "FAIL"

	if r.Pnr != "" {
		iir.Itinerary.Reference = r.Pnr + "@!c"
		iir.Itinerary.ReferenceTerm = "record_locator"
		iir.Itinerary.Name = "TakeMeThere " + r.Pnr

		if r.Event.Name != "" {
			el := LocationEvent{}
			el.Type = "activity"
			el.Name = r.Event.Name
			el.StartDateTime = r.Event.StartDate
			el.EndDateTime = r.Event.EndDate
			el.Location.Address.Line = r.Event.Address
			el.Location.Address.City = r.Event.City
			el.Location.Address.Country = r.Event.Country
			iir.Itinerary.LocationEvent = append(iir.Itinerary.LocationEvent, el)
		}

		if r.Hotel.Name != "" {
			eh := LocationEvent{}
			eh.Type = "lodging"
			eh.Name = r.Hotel.Name
			eh.StartDateTime = r.Hotel.StartDate
			eh.EndDateTime = r.Hotel.EndDate
			eh.Location.Address.Line = r.Hotel.Address
			eh.Location.Address.City = r.Hotel.City
			eh.Location.Address.Country = r.Hotel.Country
			eh.Location.Address.Postal = r.Hotel.Postal
			iir.Itinerary.LocationEvent = append(iir.Itinerary.LocationEvent, eh)
		}

		reqXml, err := xml.Marshal(&iir)
		if err != nil {
			log.Println("ERROR", err)
		}
		log.Println(string(reqXml))

		req, err := http.NewRequest("POST", cfg.Url, bytes.NewBuffer([]byte(reqXml)))
		if err != nil {
			log.Println("ERROR", err)
		}

		req.Header.Set("Content-Type", "text/xml")

		client := &http.Client{}
		res, err := client.Do(req)
		if err != nil {
			log.Println("ERROR", err)
		}
		defer res.Body.Close()

		body, _ := ioutil.ReadAll(res.Body)
		log.Println(string(body))

		response := ImportItineraryResponse{}
		err = xml.Unmarshal(body, &response)
		if err != nil {
			log.Println("ERROR", err)
		}

		if response.Success {
			result = "SUCCESS"
		}
	}

	return result
}

type Config struct {
	Token string
	Url   string
}
