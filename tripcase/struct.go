package main

import (
//"encoding/xml"
)

type TripcaseRequest struct {
	Event    SimpleLocation `json:"event"`
	Hotel    SimpleLocation `json:"hotel"`
	Pnr      string         `json:"pnr"`
	PnrEmail string         `json:"pnremail"`
}

type ImportItineraryRequest struct {
	Version     string `xml:"Version,attr"`
	UserEmail   string `xml:"UserEmail,attr"`
	PartnerCred struct {
		AuthToken string `xml:"AuthToken,attr"`
	} `xml:"PartnerCredentials"`
	Itinerary struct {
		ReferenceTerm string          `xml:"ReferenceTerm,attr"`
		Reference     string          `xml:"Reference,attr"`
		Name          string          `xml:"Name,attr"`
		LocationEvent []LocationEvent `xml:"LocationEvent"`
	} `xml:"Itinerary"`
}

type LocationEvent struct {
	Type          string `xml:"Type,attr"`
	Name          string `xml:"Name,attr"`
	StartDateTime string `xml:"StartDateTime,attr"`
	EndDateTime   string `xml:"EndDateTime,attr"`
	Location      struct {
		Address struct {
			City    string `xml:"City,attr"`
			Country string `xml:"Country,attr"`
			Postal  string `xml:"Postal,attr"`
			Line    string `xml:"Line"`
		} `xml:"Address"`
	} `xml:"Location"`
}

type SimpleLocation struct {
	Name      string `json:"name"`
	StartDate string `json:"startdate"`
	EndDate   string `json:"enddate"`
	City      string `json:"city"`
	Country   string `json:"country"`
	Address   string `json:"address"`
	Postal    string `json:"postal"`
}

type ImportItineraryResponse struct {
	Success bool `xml:"Success,attr"`
}
