package main

import (
	"strconv"
	"time"
)

const reqPattern string = `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:eb="http://www.ebxml.org/namespaces/messageHeader"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsd="http://www.w3.org/1999/XMLSchema">
    <SOAP-ENV:Header>
        <eb:MessageHeader SOAP-ENV:mustUnderstand="1" eb:version="1.0">
            <eb:ConversationId>%s</eb:ConversationId>
            <eb:From>
                <eb:PartyId type="urn:x12.org:IO5:01">%s</eb:PartyId>
            </eb:From>
            <eb:To>
                <eb:PartyId type="urn:x12.org:IO5:01">%s</eb:PartyId>
            </eb:To>
            <eb:CPAId>%s</eb:CPAId>
            <eb:Service>%s</eb:Service>
            <eb:Action>%s</eb:Action>
            <eb:MessageData>
                <eb:MessageId>%s</eb:MessageId>
                <eb:Timestamp>%s</eb:Timestamp>
            </eb:MessageData>
        </eb:MessageHeader>
        <wsse:Security
            xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext"
            xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
            <wsse:BinarySecurityToken>%s</wsse:BinarySecurityToken>
        </wsse:Security>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
        %s
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
`

type EnhancedAirBookRQ struct {
	ReturnHostCommand bool   `xml:"ReturnHostCommand,attr"`
	Version           string `xml:"Version,attr"`
	Xmlns             string `xml:"xmlns,attr"`
	//HaltOnError       bool           `xml:"HaltOnError,attr"`
	AirBook     AirBookRQ      `xml:"OTA_AirBookRQ"`
	AirPrice    AirPriceRQ     `xml:"OTA_AirPriceRQ"`
	PostProcess PostProcessing `xml:"PostProcessing"`
	PreProcess  PreProcessing  `xml:"PreProcessing"`
}

type AirBookRQ struct {
	HaltOnStatus                 []HaltOnStatus `xml:"HaltOnStatus"`
	OriginDestinationInformation struct {
		FlightSegment []AirBookRQ_FlightSegment `xml:"FlightSegment"`
	} `xml:"OriginDestinationInformation"`
	Redisplay struct {
		NumAttempts  int `xml:"NumAttempts,attr"`
		WaitInterval int `xml:"WaitInterval,attr"`
	} `xml:"RedisplayReservation"`
}

type HaltOnStatus struct {
	Code string `xml:"Code,attr"`
}

type AirBookRQ_FlightSegment struct {
	ArrivalDateTime     string `xml:"ArrivalDateTime,attr"`
	DepartureDateTime   string `xml:"DepartureDateTime,attr"`
	FlightNumber        int    `xml:"FlightNumber,attr"`
	NumberInParty       int    `xml:"NumberInParty,attr"`
	ResBookDesigCode    string `xml:"ResBookDesigCode,attr"`
	Status              string `xml:"Status,attr"`
	DestinationLocation struct {
		LocationCode string `xml:"LocationCode,attr"`
	} `xml:"DestinationLocation"`
	MarketingAirline struct {
		Code         string `xml:"Code,attr"`
		FlightNumber int    `xml:"FlightNumber,attr"`
	} `xml:"MarketingAirline"`
	MarriageGrp struct {
		Ind bool `xml:"Ind,attr"`
	} `xml:"MarriageGrp"`
	OriginLocation struct {
		LocationCode string `xml:"LocationCode,attr"`
	} `xml:"OriginLocation"`
}

type AirPriceRQ struct {
	PriceComparison struct {
		AmountSpecified float64 `xml:"AmountSpecified,attr"`
	} `xml:"PriceComparison"`
	PriceRequestInformation struct {
		OptionalQualifiers struct {
			PricingQualifiers struct {
				Currency      string `xml:"CurrencyCode,attr"`
				PassengerType struct {
					Code     string `xml:"Code,attr"`
					Quantity int    `xml:"Quantity,attr"`
				}
			} `xml:"PricingQualifiers"`
		} `xml:"OptionalQualifiers"`
	} `xml:"PriceRequestInformation"`
}

type PostProcessing struct {
	IgnoreAfter bool `xml:"IgnoreAfter,attr"`
	Redisplay   struct {
		WaitInterval int `xml:"WaitInterval,attr"`
	} `xml:"RedisplayReservation"`
}

type PreProcessing struct {
	IgnoreBefore bool `xml:"IgnoreBefore,attr"`
}

func (e *EnhancedAirBookRQ) ImportOffer(o PricedItinerary, pcnt int) {
	e.ReturnHostCommand = true
	e.Version = "2.5.0"
	e.Xmlns = "http://webservices.sabre.com/sabreXML/2011/10"

	e.PreProcess.IgnoreBefore = true
	e.PostProcess.Redisplay.WaitInterval = 5000

	e.AirPrice.PriceComparison.AmountSpecified, _ = strconv.ParseFloat(o.PriceInfo.ItinTotalFare.TotalFare.Amount, 64)
	e.AirPrice.PriceRequestInformation.OptionalQualifiers.PricingQualifiers.Currency = o.PriceInfo.ItinTotalFare.TotalFare.Currency
	e.AirPrice.PriceRequestInformation.OptionalQualifiers.PricingQualifiers.PassengerType.Code = "ADT"
	e.AirPrice.PriceRequestInformation.OptionalQualifiers.PricingQualifiers.PassengerType.Quantity = pcnt

	e.AirBook.HaltOnStatus = append(e.AirBook.HaltOnStatus, HaltOnStatus{Code: "NO"}, HaltOnStatus{Code: "NN"}, HaltOnStatus{Code: "UC"}, HaltOnStatus{Code: "US"})
	e.AirBook.Redisplay.NumAttempts = 5
	e.AirBook.Redisplay.WaitInterval = 5000
	for _, od := range o.FlightInfo.OriginDestinationOptions.OriginDestinationOption {
		for _, fs := range od.FlightSegments {
			abfs := AirBookRQ_FlightSegment{}
			abfs.ArrivalDateTime = fs.ArrivalTime[:16]
			abfs.DepartureDateTime = fs.DepartureTime[:16]
			abfs.FlightNumber = fs.FlightNumber
			abfs.NumberInParty = pcnt
			abfs.ResBookDesigCode = fs.FlightClass
			abfs.Status = "NN"
			abfs.DestinationLocation.LocationCode = fs.ArrivalAirport.Code
			abfs.MarketingAirline.Code = fs.MarketingAirline.Code
			abfs.MarketingAirline.FlightNumber = fs.FlightNumber
			if fs.Marriage == "O" {
				abfs.MarriageGrp.Ind = true
			}
			abfs.OriginLocation.LocationCode = fs.DepartureAirport.Code
			e.AirBook.OriginDestinationInformation.FlightSegment = append(e.AirBook.OriginDestinationInformation.FlightSegment, abfs)
		}
	}
}

type PassengerDetailsRQ struct {
	Version        string `xml:"version,attr"`
	Xmlns          string `xml:"xmlns,attr"`
	HaltOnError    bool   `xml:"HaltOnError,attr"`
	PostProcessing struct {
		IgnoreAfter          bool `xml:"IgnoreAfter,attr"`
		RedisplayReservation bool `xml:"RedisplayReservation,attr"`
		EndTransactionRQ     struct {
			EndTransaction struct {
				Ind   bool `xml:"Ind,attr"`
				Email struct {
					Ind bool `xml:"Ind,attr"`
					//					Eticket struct {
					//						Ind bool `xml:"Ind,attr"`
					//						Pdf struct {
					//							Ind bool `xml:"Ind,attr"`
					//						} `xml:"PDF"`
					//					} `xml:"eTicket"`
					PersonName struct {
						NameNumber string `xml:"NameNumber,attr"`
					} `xml:"PersonName"`
				} `xml:"Email"`
			} `xml:"EndTransaction"`
			Source struct {
				ReceivedFrom string `xml:"ReceivedFrom,attr"`
			} `xml:"Source"`
		} `xml:"EndTransactionRQ"`
	} `xml:"PostProcessing"`
	//	PriceQuoteInfo struct {
	//		Link []Link `xml:"Link"`
	//	} `xml:"PriceQuoteInfo"`
	SpecialReqDetails struct {
		AddRemarkRQ struct {
			RemarkInfo struct {
				FOP_Remark struct {
					Type string `xml:"Type,attr"`
				} `xml:"FOP_Remark"`
			} `xml:"RemarkInfo"`
		} `xml:"AddRemarkRQ"`
	} `xml:"SpecialReqDetails"`
	TravelItineraryAddInfoRQ struct {
		AgencyInfo struct {
			Ticketing struct {
				PseudoCityCode  string `xml:"PseudoCityCode,attr"`
				TicketTimeLimit string `xml:"TicketTimeLimit,attr"`
				TicketType      string `xml:"TicketType,attr"`
			} `xml:"Ticketing"`
		} `xml:"AgencyInfo"`
		CustomerInfo struct {
			ContactNumbers struct {
				ContactNumber []ContactNumber `xml:"ContactNumber"`
			} `xml:"ContactNumbers"`
			Email      []Email      `xml:"Email"`
			PersonName []PersonName `xml:"PersonName"`
		} `xml:"CustomerInfo"`
	} `xml:"TravelItineraryAddInfoRQ"`
}

type Link struct {
	NameNumber string `xml:"NameNumber,attr"`
	Record     int    `xml:"Record,attr"`
}

type ContactNumber struct {
	NameNumber   string `xml:"NameNumber,attr"`
	Phone        string `xml:"Phone,attr"`
	PhoneUseType string `xml:"PhoneUseType,attr"`
}

type Email struct {
	Address    string `xml:"Address,attr"`
	NameNumber string `xml:"NameNumber,attr"`
	Type       string `xml:"Type,attr"`
}

type PersonName struct {
	NameNumber    string `xml:"NameNumber,attr"`
	PassengerType string `xml:"PassengerType,attr"`
	GivenName     string `xml:"GivenName"`
	Surname       string `xml:"Surname"`
}

func (p *PassengerDetailsRQ) ImportPersons(bp []BookPerson) {
	p.Version = "3.1.0"
	p.Xmlns = "http://services.sabre.com/sp/pd/v3_1"
	p.HaltOnError = true

	p.PostProcessing.IgnoreAfter = true
	p.PostProcessing.RedisplayReservation = true
	p.PostProcessing.EndTransactionRQ.EndTransaction.Ind = true
	p.PostProcessing.EndTransactionRQ.EndTransaction.Email.Ind = true

	p.PostProcessing.EndTransactionRQ.Source.ReceivedFrom = "TAKEMETHERE"

	p.SpecialReqDetails.AddRemarkRQ.RemarkInfo.FOP_Remark.Type = "CASH"

	p.TravelItineraryAddInfoRQ.AgencyInfo.Ticketing.PseudoCityCode = cfgBook.Client.Ipcc
	p.TravelItineraryAddInfoRQ.AgencyInfo.Ticketing.TicketTimeLimit = time.Now().Add(24 * time.Hour).Format(time.RFC3339)[5:14] + "00"
	p.TravelItineraryAddInfoRQ.AgencyInfo.Ticketing.TicketType = "7TAW"

	//var pdll []Link
	var pdlc []ContactNumber
	var pdle []Email
	var pdlp []PersonName

	contactEmail := ""

	if len(bp) > 0 {
		for k, v := range bp {
			nn := strconv.Itoa(k+1) + ".1"

			if v.Phone != "" {
				c := ContactNumber{NameNumber: nn, Phone: v.Phone, PhoneUseType: "H"}
				pdlc = append(pdlc, c)
			}

			if v.Email != "" {
				if contactEmail == "" {
					contactEmail = nn
				}
				e := Email{NameNumber: nn, Address: v.Email, Type: "CC"}
				pdle = append(pdle, e)
			}

			p := PersonName{NameNumber: nn, PassengerType: "ADT", GivenName: v.Name, Surname: v.LastName}
			pdlp = append(pdlp, p)
		}

		p.PostProcessing.EndTransactionRQ.EndTransaction.Email.PersonName.NameNumber = contactEmail

		p.TravelItineraryAddInfoRQ.CustomerInfo.ContactNumbers.ContactNumber = pdlc
		p.TravelItineraryAddInfoRQ.CustomerInfo.Email = pdle
		p.TravelItineraryAddInfoRQ.CustomerInfo.PersonName = pdlp
	}
}

type BookResponse struct {
	Success  bool        `json:"Success"`
	Pnr      string      `json:"PNR"`
	PnrEmail string      `json:"PNRemail"`
	Offer    ParsedOffer `json:"Offer"`
}
