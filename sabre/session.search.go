package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

var sl SessionSearchList
var slWaitGroup sync.WaitGroup

type SessionSearchList struct {
	List []*SessionSearch
	Lock sync.Mutex
}

type SessionSearchRes struct {
	Success bool
	Result  SessionSearch
}

type SessionSearch struct {
	Token    string `json:"access_token"`
	Type     string `json:"token_type"`
	Expire   int    `json:"expires_in"`
	Deadline time.Time
}

func SessionSearchInit() {
	for i := 0; i < cfgSearch.SessionInit; i++ {
		s := SessionSearchOpen()
		SessionSearchRelease(&s)
	}

	slWaitGroup.Add(1)
	go SessionSearchClear()
}

func SessionSearchOpen() SessionSearch {
	url := cfgSearch.SessionUrl + "?" +
		"apiId=" + cfgSearch.ApiId +
		"&auth_flow=" + cfgSearch.AuthFlow +
		"&client_id=" + cfgSearch.Client.Id +
		"&client_secret=" + cfgSearch.Client.Secret

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println("ERROR", err)
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("ERROR", err)
	}
	defer res.Body.Close()

	t := time.Now()
	body, _ := ioutil.ReadAll(res.Body)

	r := SessionSearchRes{}
	err = json.Unmarshal(body, &r)
	if err != nil {
		log.Println("ERROR", err)
	}

	r.Result.Deadline = t.Add(time.Duration(r.Result.Expire) * time.Second)

	log.Println("SESSION", "search opened")
	return r.Result
}

func SessionSearchGet() SessionSearch {
	s := SessionSearch{}

	if len(sl.List) > 0 {
		sl.Lock.Lock()
		s = *sl.List[0]
		sl.List = sl.List[1:]
		sl.Lock.Unlock()
	} else {
		s = SessionSearchOpen()
	}

	log.Println("SESSION", "search returned")
	log.Println("SESSION", "search list length", len(sl.List))
	return s
}

func SessionSearchRelease(s *SessionSearch) {
	sl.Lock.Lock()
	sl.List = append(sl.List, s)
	log.Println("SESSION", "search released")
	sl.Lock.Unlock()

	log.Println("SESSION", "search list length", len(sl.List))
}

func SessionSearchClear() {
	for {
	AfterClear:
		for k, v := range sl.List {
			if time.Now().After(v.Deadline) {
				sl.Lock.Lock()
				sl.List = append(sl.List[:k], sl.List[k+1:]...)

				if len(sl.List) < cfgSearch.SessionInit {
					s := SessionSearchOpen()
					sl.List = append(sl.List, &s)
				}
				sl.Lock.Unlock()

				log.Println("SESSION", "search removed")
				goto AfterClear
			}
		}
		log.Println("SESSION", "search list length", len(sl.List))
		time.Sleep(5 * time.Second)
	}

	slWaitGroup.Done()
}
