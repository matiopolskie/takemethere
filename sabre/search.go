package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

var RawResults map[string]PricedItinerary
var NiceResults map[string]ParsedOffer

type SearchRequest struct {
	DepCode string
	ArrCode string
	DepDate string
	RetDate string
	Persons int
}

func SearchHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var req SearchRequest
	var res string
	err := decoder.Decode(&req)
	if err != nil {
		res = "{}"
		log.Println("ERROR", "parsing JSON request")
	} else {
		s := SessionSearchGet()
		res = s.SearchInstaFlight(&req)
		SessionSearchRelease(&s)
	}

	fmt.Fprintf(w, res)
}

func (s *SessionSearch) SearchInstaFlight(r *SearchRequest) string {
	url := cfgSearch.SearchUrl + "?" +
		"origin=" + r.DepCode +
		"&destination=" + r.ArrCode +
		"&departuredate=" + r.DepDate +
		"&returndate=" + r.RetDate +
		"&onlineitinerariesonly=Y" +
		"&limit=" + strconv.Itoa(cfgSearch.SearchResults) +
		"&offset=1" +
		"&eticketsonly=Y" +
		"&sortby=totalfare&order=asc&sortby2=departuretime&order2=asc" +
		"&pointofsalecountry=" + cfgSearch.SearchPOS +
		"&passengercount=" + strconv.Itoa(r.Persons)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println("ERROR", err)
	}

	req.Header.Set("Authorization", s.Type+" "+s.Token)
	//req.Header.Add("Accept-Encoding", "gzip")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("ERROR", err)
	}
	defer res.Body.Close()

	log.Println("SEARCH", res.Status)

	switch res.StatusCode {
	case 404: // not found
		log.Println("SEARCH", "no results found")
		response := SearchResponse{}
		resJson, err := json.Marshal(response)
		if err != nil {
			log.Println("ERROR", err)
		}

		log.Println(string(resJson))
		return string(resJson)
	case 200: // results
		switch res.Header.Get("Content-Encoding") {
		case "gzip":
			break
		}
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("ERROR", err)
	}

	sres := SearchResult{}
	err = json.Unmarshal(body, &sres)
	if err != nil {
		log.Println("ERROR", err)
	}

	response := SearchResponse{}
	if len(sres.PricedItineraries) > 0 {
		for _, v := range sres.PricedItineraries {
			parsedOffer := v.ParseToOffer(r.Persons)
			RawResults[parsedOffer.Hash] = v
			NiceResults[parsedOffer.Hash] = parsedOffer
			response.OfferList = append(response.OfferList, parsedOffer)
		}
	}

	resJson, err := json.Marshal(response)
	if err != nil {
		log.Println("ERROR", err)
	}

	return string(resJson)
}
