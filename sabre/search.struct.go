package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
)

/* ORIGINAL RESPONSE: START */

type SearchResult struct {
	PricedItineraries []PricedItinerary `json:"PricedItineraries"`
}

type PricedItinerary struct {
	FlightInfo AirItinerary            `json:"AirItinerary"`
	PriceInfo  AirItineraryPricingInfo `json:"AirItineraryPricingInfo"`
}

type AirItinerary struct {
	OriginDestinationOptions OriginDestinationOptions `json:"OriginDestinationOptions"`
}

type OriginDestinationOptions struct {
	OriginDestinationOption []OriginDestinationOption `json:"OriginDestinationOption"`
}

type OriginDestinationOption struct {
	FlightSegments []FlightSegment `json:"FlightSegment"`
}

type FlightSegment struct {
	DepartureAirport LocationCode
	ArrivalAirport   LocationCode
	MarketingAirline SimpleCode
	FlightNumber     int
	ElapsedTime      int
	FlightClass      string `json:"ResBookDesigCode"`
	Marriage         string `json:"MarriageGrp"`
	DepartureTime    string `json:"DepartureDateTime"`
	ArrivalTime      string `json:"ArrivalDateTime"`
}

type LocationCode struct {
	Code string `json:"LocationCode"`
}

type SimpleCode struct {
	Code string `json:"Code"`
}

type AirItineraryPricingInfo struct {
	ItinTotalFare ItinTotalFare `json:"ItinTotalFare"`
}

type ItinTotalFare struct {
	TotalFare FarePrice `json:"TotalFare"`
}

type FarePrice struct {
	Currency string `json:"CurrencyCode"`
	Amount   string `json:"Amount"`
}

/* ORIGINAL RESPONSE: END */
/* PARSED RESPONSE: START */

type SearchResponse struct {
	OfferList []ParsedOffer
}

type ParsedOffer struct {
	Outbound  []Flight
	Inbound   []Flight
	TotalFare FarePrice
	Hash      string
}

type Flight struct {
	DepCode      string
	ArrCode      string
	DepDate      string
	DepTime      string
	ArrDate      string
	ArrTime      string
	Airline      string
	FlightNumber string
}

func (p *PricedItinerary) ParseToOffer(pcnt int) ParsedOffer {
	po := ParsedOffer{}
	for _, v := range p.FlightInfo.OriginDestinationOptions.OriginDestinationOption[0].FlightSegments {
		f := Flight{}
		f.DepCode = v.DepartureAirport.Code
		f.ArrCode = v.ArrivalAirport.Code

		ddt := strings.Split(v.DepartureTime, "T")
		f.DepDate = ddt[0]
		f.DepTime = ddt[1][:5]

		adt := strings.Split(v.ArrivalTime, "T")
		f.ArrDate = adt[0]
		f.ArrTime = adt[1][:5]

		f.Airline = v.MarketingAirline.Code
		f.FlightNumber = strconv.Itoa(v.FlightNumber)

		po.Outbound = append(po.Outbound, f)
	}

	for _, v := range p.FlightInfo.OriginDestinationOptions.OriginDestinationOption[1].FlightSegments {
		f := Flight{}
		f.DepCode = v.DepartureAirport.Code
		f.ArrCode = v.ArrivalAirport.Code

		ddt := strings.Split(v.DepartureTime, "T")
		f.DepDate = ddt[0]
		f.DepTime = ddt[1][:5]

		adt := strings.Split(v.ArrivalTime, "T")
		f.ArrDate = adt[0]
		f.ArrTime = adt[1][:5]

		f.Airline = v.MarketingAirline.Code
		f.FlightNumber = strconv.Itoa(v.FlightNumber)

		po.Inbound = append(po.Inbound, f)
	}

	po.TotalFare = p.PriceInfo.ItinTotalFare.TotalFare

	hash := ""
	oPart, _ := json.Marshal(po.Outbound)
	iPart, _ := json.Marshal(po.Inbound)
	hash += fmt.Sprintf("%x", md5.Sum(oPart)) + fmt.Sprintf("%x", md5.Sum(iPart)) + "_" + strconv.Itoa(pcnt)

	po.Hash = hash
	return po
}

/* PARSED RESPONSE: END */
