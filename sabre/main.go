package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
)

var cfgSearch ConfigSearch
var cfgBook ConfigBook

func init() {
	log.SetFlags(log.Lshortfile | log.Ltime | log.Ldate)

	file, _ := os.Open("cfg.search.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&cfgSearch)
	if err != nil {
		log.Println("ERROR", err)
	} else {
		log.Println("INIT", "config search read")
	}

	SessionSearchInit()

	RawResults = make(map[string]PricedItinerary)
	NiceResults = make(map[string]ParsedOffer)

	file, _ = os.Open("cfg.book.json")
	decoder = json.NewDecoder(file)
	err = decoder.Decode(&cfgBook)
	if err != nil {
		log.Println("ERROR", err)
	} else {
		log.Println("INIT", "config book read")
	}

	SessionBookInit()
}

func main() {
	log.Println("MAIN START")

	http.HandleFunc("/search/", SearchHandler)
	http.HandleFunc("/book/", BookHandler)
	http.ListenAndServe(":7777", nil)

	slWaitGroup.Wait()
	slbWaitGroup.Wait()

	log.Println("MAIN END")
}

type ConfigSearch struct {
	Client        ClientSearch
	ApiId         string
	AuthFlow      string
	SessionUrl    string
	SessionMax    int
	SessionInit   int
	SearchUrl     string
	SearchPOS     string
	SearchResults int
}

type ClientSearch struct {
	Id     string
	Secret string
}

type ConfigBook struct {
	Client      ClientBook
	SessionUrl  string
	SessionMax  int
	SessionInit int
	BookUrl     string
}

type ClientBook struct {
	User string
	Pass string
	Ipcc string
	From string
	To   string
}
