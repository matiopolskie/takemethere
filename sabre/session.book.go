package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"sync"
	"time"
)

var slb SessionBookList
var slbWaitGroup sync.WaitGroup

type SessionBookList struct {
	List []*SessionBook
	Lock sync.Mutex
}

type SessionBookRes struct {
	Success bool
	Result  SessionBook
}

type SessionBook struct {
	Token    string
	Expire   int
	Deadline time.Time
}

func SessionBookInit() {
	for i := 0; i < cfgBook.SessionInit; i++ {
		s := SessionBookOpen()
		SessionBookRelease(&s)
	}

	slbWaitGroup.Add(1)
	go SessionBookClear()
}

func SessionBookOpen() SessionBook {
	reqPattern := `<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:eb="http://www.ebxml.org/namespaces/messageHeader"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsd="http://www.w3.org/1999/XMLSchema">
    <SOAP-ENV:Header>
        <eb:MessageHeader SOAP-ENV:mustUnderstand="1" eb:version="1.0">
            <eb:From>
                <eb:PartyId type="urn:x12.org:IO5:01">%s</eb:PartyId>
            </eb:From>
            <eb:To>
                <eb:PartyId type="urn:x12.org:IO5:01">%s</eb:PartyId>
            </eb:To>
            <eb:CPAId>%s</eb:CPAId>
            <eb:ConversationId>%s</eb:ConversationId>
            <eb:Service eb:type="OTA">SessionCreateRQ</eb:Service>
            <eb:Action>SessionCreateRQ</eb:Action>
            <eb:MessageData>
                <eb:MessageId>%s</eb:MessageId>
                <eb:Timestamp>%s</eb:Timestamp>
            </eb:MessageData>
        </eb:MessageHeader>
        <wsse:Security
            xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/12/secext"
            xmlns:wsu="http://schemas.xmlsoap.org/ws/2002/12/utility">
            <wsse:UsernameToken>
                <wsse:Username>%s</wsse:Username>
                <wsse:Password>%s</wsse:Password>
                <Organization>%s</Organization>
                <Domain>DEFAULT</Domain>
            </wsse:UsernameToken>
        </wsse:Security>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
        <SessionCreateRQ returnContextID="true">
            <POS>
                <Source PseudoCityCode="%s"/>
            </POS>
        </SessionCreateRQ>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>`

	reqXml := fmt.Sprintf(reqPattern,
		cfgBook.Client.From,
		cfgBook.Client.To,
		cfgBook.Client.Ipcc,
		time.Now().Format(time.RFC3339Nano)+"_SESOPN",
		time.Now().Format(time.RFC3339Nano)+"_MSGID",
		time.Now().Format(time.RFC3339),
		cfgBook.Client.User,
		cfgBook.Client.Pass,
		cfgBook.Client.Ipcc,
		cfgBook.Client.Ipcc)

	req, err := http.NewRequest("POST", cfgBook.SessionUrl, bytes.NewBuffer([]byte(reqXml)))
	if err != nil {
		log.Println("ERROR", err)
	}

	req.Header.Set("Content-Type", "text/xml")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("ERROR", err)
	}
	defer res.Body.Close()

	t := time.Now()
	body, _ := ioutil.ReadAll(res.Body)

	reg := regexp.MustCompile(`(?m)<wsse:BinarySecurityToken(.*)?>(.+)?<\/wsse:BinarySecurityToken>`)
	tkn := reg.FindStringSubmatch(string(body))

	r := SessionBookRes{}

	r.Success = true
	r.Result.Token = tkn[2]
	r.Result.Expire = 900
	r.Result.Deadline = t.Add(time.Duration(r.Result.Expire) * time.Second)

	log.Println("SESSION", "book opened")
	return r.Result
}

func SessionBookGet() SessionBook {
	s := SessionBook{}

	if len(slb.List) > 0 {
		slb.Lock.Lock()
		s = *slb.List[0]
		slb.List = slb.List[1:]
		slb.Lock.Unlock()
	} else {
		s = SessionBookOpen()
	}

	log.Println("SESSION", "book returned")
	log.Println("SESSION", "book list length", len(slb.List))
	return s
}

func SessionBookRelease(s *SessionBook) {
	slb.Lock.Lock()
	slb.List = append(slb.List, s)
	log.Println("SESSION", "book released")
	slb.Lock.Unlock()

	log.Println("SESSION", "book list length", len(slb.List))
}

func SessionBookClear() {
	for {
	AfterClear:
		for k, v := range slb.List {
			if time.Now().After(v.Deadline) {
				slb.Lock.Lock()
				slb.List = append(slb.List[:k], slb.List[k+1:]...)

				if len(slb.List) < cfgBook.SessionInit {
					s := SessionBookOpen()
					slb.List = append(slb.List, &s)
				}
				slb.Lock.Unlock()

				log.Println("SESSION", "book removed")
				goto AfterClear
			}
		}
		log.Println("SESSION", "book list length", len(slb.List))
		time.Sleep(5 * time.Second)
	}

	slbWaitGroup.Done()
}

func (s *SessionBook) SessionBookUpdate() {
	s.Deadline = time.Now().Add(time.Duration(s.Expire) * time.Second)
}
