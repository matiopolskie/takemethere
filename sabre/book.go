package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"time"
)

type BookRequest struct {
	Hash    string
	Persons []BookPerson
}

type BookPerson struct {
	Name      string
	LastName  string
	BirthDate string
	Email     string
	Phone     string
}

func BookHandler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var req BookRequest
	var res string
	err := decoder.Decode(&req)
	if err != nil {
		res = "{}"
		log.Println("ERROR", "parsing JSON request")
	} else {
		s := SessionBookGet()
		res = s.Book(&req)
		SessionBookRelease(&s)
	}

	fmt.Fprintf(w, res)
}

func (s *SessionBook) Book(r *BookRequest) string {
	offer := RawResults[r.Hash]

	eab := EnhancedAirBookRQ{}
	eab.ImportOffer(offer, len(r.Persons))
	eabXml, _ := xml.Marshal(&eab)

	reqXml := fmt.Sprintf(reqPattern,
		time.Now().Format(time.RFC3339Nano)+"_CONVID",
		cfgBook.Client.From,
		cfgBook.Client.To,
		cfgBook.Client.Ipcc,
		"",
		"EnhancedAirBookRQ",
		time.Now().Format(time.RFC3339Nano)+"_MSGID",
		time.Now().Format(time.RFC3339)[:19],
		s.Token,
		string(eabXml))

	req, err := http.NewRequest("POST", cfgBook.BookUrl, bytes.NewBuffer([]byte(reqXml)))
	if err != nil {
		log.Println("ERROR", err)
	}

	req.Header.Set("Content-Type", "text/xml")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("ERROR", err)
	}
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	// jak nie ma bledu, to wyslij dane uzytkownikow albo pierdol wszystko

	pd := PassengerDetailsRQ{}
	pd.ImportPersons(r.Persons)
	pdXml, _ := xml.Marshal(&pd)

	reqXml = fmt.Sprintf(reqPattern,
		time.Now().Format(time.RFC3339Nano)+"_CONVID",
		cfgBook.Client.From,
		cfgBook.Client.To,
		cfgBook.Client.Ipcc,
		"",
		"PassengerDetailsRQ",
		time.Now().Format(time.RFC3339Nano)+"_MSGID",
		time.Now().Format(time.RFC3339)[:19],
		s.Token,
		string(pdXml))

	req, err = http.NewRequest("POST", cfgBook.BookUrl, bytes.NewBuffer([]byte(reqXml)))
	if err != nil {
		log.Println("ERROR", err)
	}

	req.Header.Set("Content-Type", "text/xml")

	res, err = client.Do(req)
	if err != nil {
		log.Println("ERROR", err)
	}
	defer res.Body.Close()

	body, _ = ioutil.ReadAll(res.Body)

	reg := regexp.MustCompile(`(?m)<ItineraryRef ID="(.+)?"\/>`)
	pnr := reg.FindStringSubmatch(string(body))

	br := BookResponse{}
	br.Success = true
	br.Offer = NiceResults[r.Hash]

	for _, v := range r.Persons {
		if v.Email != "" {
			br.PnrEmail = v.Email
			break
		}
	}

	if len(pnr) > 0 {
		br.Pnr = pnr[1]
	} else {
		br.Pnr = "ABCDEF"
	}

	resJson, err := json.Marshal(&br)
	if err != nil {
		log.Println("ERROR", err)
	}
	return string(resJson)
}
