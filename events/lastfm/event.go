package lastfm

import (
	"encoding/json"
	s "events/struct"
	"log"
)

func (lfi *LastFmInstance) EventInfo(id, artistid string, userlat, userlng string) s.EventsStruct {
	url := buildUrl("event.getInfo", "event="+id)
	resp := getResponse(url)
	respParse := LFOneEvent{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}
	htl, _ := lfi.parseEventInfo(respParse.Event, artistid, userlat, userlng, true)
	lfi.listEvents[htl.Id] = htl
	log.Println("LIST", lfi.listEvents)
	return htl
}
