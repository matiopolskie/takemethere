package lastfm

import (
	"encoding/json"
	s "events/struct"
	"log"
	"strconv"
	"strings"
	"time"
)

func (lfi *LastFmInstance) SearchEvents(id string) []s.EventsStruct {
	url := buildUrl("artist.getEvents", "mbid="+id)
	x := getResponse(url)
	y := lfi.parseEvents(x, id, false)
	return y
}

func (lfi *LastFmInstance) parseEvents(resp []byte, id string, extendinfo bool) []s.EventsStruct {
	respParse := LFEventsResp{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR Events 1 ", err)
	}

	var x []s.EventsStruct
	for _, r := range respParse.Events.Event {
		htl, b := lfi.parseEventInfo(r, id, "", "", false)
		if b {
			x = append(x, htl)
		}
	}
	return x
}

func (lfi *LastFmInstance) parseEventInfo(r LFEvent, id string, userlat, userlng string, extendinfo bool) (s.EventsStruct, bool) {

	y := s.EventsStruct{}
	if r.Venue.Location.Point.Lat == "" || r.Venue.Location.Point.Lng == "" {
		return y, false
	}
	y.Id = "LF" + r.Id
	y.Title = r.Title
	if r.StartDate != "" {
		t1, err := time.Parse("Mon, 02 Jan 2006 15:04:05", r.StartDate)
		if err != nil {
			log.Println("ERR Events 2 DATE ", err)
		} else {
			y.StartdateTime = t1
			y.Startdate = t1.Format("2006-01-02 15:04:05")
		}
	}
	if r.EndDate != "" {
		t1, err := time.Parse("Mon, 02 Jan 2006 15:04:05", r.EndDate)
		if err != nil {
			log.Println("ERR Events 2 DATE ", err)
		} else {
			y.Enddate = t1.Format("2006-01-02 15:04:05")
		}
	}
	y.Img = findImage(r.Img)
	y.Lat = r.Venue.Location.Point.Lat
	y.Lng = r.Venue.Location.Point.Lng
	y.City = r.Venue.Location.City
	y.Performers = ""
	switch r.Artists.Artist.(type) {
	case string:
		y.Performers = r.Artists.Artist.(string)
	default:
		if len(r.Artists.Artist.([]interface{})) > 0 {
			first := true
			for _, a := range r.Artists.Artist.([]interface{}) {
				if !first {
					y.Performers += ", "
				}
				first = false
				y.Performers += a.(string)
			}
		}
	}
	id = strings.Replace(id, "LF", "", -1)
	log.Println("ID ", id)
	log.Println("lfi.listArtists ", lfi.listArtists)
	y.ArtistSearchName = lfi.listArtists[id]
	y.ArtistSearchId = "LF" + id

	lat, err1 := strconv.ParseFloat(y.Lat, 64)
	lng, err2 := strconv.ParseFloat(y.Lng, 64)
	if err1 != nil || err2 != nil {
		log.Println("GPS LF LNG ", err1, err2)
	} else {
		y.DestinationCode, y.DestinationDesc = lfi.airport.GetNearAirport(lat, lng)
	}

	if userlat != "" && userlng != "" {
		lat, err1 := strconv.ParseFloat(userlat, 64)
		lng, err2 := strconv.ParseFloat(userlng, 64)
		if err1 != nil || err2 != nil {
			log.Println("GPS LF LNG ", err1, err2)
		} else {
			y.DepartureCode, y.DepartureDesc = lfi.airport.GetNearAirport(lat, lng)
		}
	}

	if extendinfo {
		y.Description = r.Description
	}
	return y, true
}

func findImage(list []Images) string {
	var url string
	size := 0
	// mega = 5
	// extralarge = 4
	// large = 3
	// medium = 2
	// small = 1
	if len(list) > 0 {
		for _, i := range list {
			if i.Size == "extralarge" || i.Size == "mega" {
				return i.Img
			} else if i.Size == "large" && size < 3 {
				url = i.Img
				size = 3
			} else if i.Size == "medium" && size < 2 {
				url = i.Img
				size = 2
			} else if i.Size == "small" && size < 1 {
				url = i.Img
				size = 1
			}

		}
	}
	return url
}
