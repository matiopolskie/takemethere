package lastfm

import (
	"encoding/json"
	s "events/struct"
	"log"
)

func (lfi *LastFmInstance) SearchArtists(artist string) s.ResponseArtists {
	url := buildUrl("artist.search", "artist="+artist)
	x := getResponse(url)
	y := lfi.parseArtists(x)
	return y
}

func (lfi *LastFmInstance) parseArtists(resp []byte) s.ResponseArtists {
	respParse := LFArtists{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}

	x := s.ResponseArtists{}
	if respParse.Results.Total != "0" && respParse.Results.Total != "1" {
		for _, a := range respParse.Results.Artistmatches.Artist {
			if a.Id != "" {
				lfi.listArtists[a.Id] = a.Name
				y := s.ArtistData{}
				y.Id = "LF" + a.Id
				y.Name = a.Name
				x.Artists = append(x.Artists, y)
			}
		}
	} else {
		respParseOne := LFArtistsOne{}
		if err := json.Unmarshal(resp, &respParseOne); err != nil {
			log.Println("ERR ", err)
		}
		art2 := respParseOne.Results.Artistmatches.Artist
		if art2.Id != "" {
			y := s.ArtistData{}
			y.Id = "LF" + art2.Id
			y.Name = art2.Name
			x.Artists = append(x.Artists, y)
		}
	}
	return x
}
