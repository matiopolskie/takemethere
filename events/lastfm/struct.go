package lastfm

type LFArtists struct {
	Results LFResults `json:"results"`
}
type LFArtistsOne struct {
	Results LFResultsOne `json:"results"`
}
type LFTopArtists struct {
	Results LFArtistmatches `json:"topartists"`
}
type LFResults struct {
	Total         string          `json:"opensearch:totalResults"`
	Artistmatches LFArtistmatches `json:"artistmatches"`
}
type LFResultsOne struct {
	Total         string             `json:"opensearch:totalResults"`
	Artistmatches LFArtistmatchesOne `json:"artistmatches"`
}
type LFArtistmatches struct {
	Artist []LFArtist `json:"artist"`
}
type LFArtistmatchesOne struct {
	Artist LFArtist `json:"artist"`
}

type LFArtist struct {
	Id   string `json:"mbid"`
	Name string `json:"name"`
}

type LFEventsResp struct {
	Events LFEvents `json:"events"`
}
type LFEvents struct {
	Event []LFEvent `json:"event"`
}

type LFOneEvent struct {
	Event LFEvent `json:"event"`
}

type LFEvent struct {
	Id          string        `json:"id"`
	Artists     LFArtistsList `json:"artists"`
	Title       string        `json:"title"`
	StartDate   string        `json:"startDate"`
	EndDate     string        `json:"endDate"`
	Description string        `json:"description"`
	Img         []Images      `json:"image"`
	Venue       LFVenue       `json:"venue"`
}
type LFArtistsList struct {
	Artist interface{} `json:"artist"`
}

type Images struct {
	Img  string `json:"#text"`
	Size string `json:"size"`
}

type LFVenue struct {
	Location LFLocation `json:"location"`
}

type LFLocation struct {
	City  string  `json:"city"`
	Point LFPoint `json:"geo:point"`
}

type LFPoint struct {
	Lat string `json:"geo:lat"`
	Lng string `json:"geo:long"`
}
