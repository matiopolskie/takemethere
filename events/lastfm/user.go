package lastfm

import (
	"encoding/json"
	s "events/struct"
	"log"
)

func (lfi *LastFmInstance) GetUserArtists(name string) s.ResponseArtists {
	url := buildUrl("user.getTopArtists", "user="+name)
	resp := getResponse(url)
	log.Println(string(resp))
	respParse := LFTopArtists{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}
	log.Println(respParse)
	x := s.ResponseArtists{}

	for _, a := range respParse.Results.Artist {
		if a.Id != "" {
			x.Artists = append(x.Artists, s.ArtistData{Id: "LF" + a.Id, Name: a.Name})
		}
	}

	// htl, _ := lfi.parseEventInfo(respParse.Event, artistid, userlat, userlng, true)
	return x
}
