package lastfm

import (
	a "events/airports"
	s "events/struct"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var urlBase string = `http://ws.audioscrobbler.com/2.0/?api_key=XXXXXXXXXXXXXXXXXXX&format=json&`

type LastFmInstance struct {
	listArtists map[string]string
	airport     *a.AirportInstance
	listEvents  map[string]s.EventsStruct
}

func Init(ai *a.AirportInstance) *LastFmInstance {
	x := &LastFmInstance{}
	x.listArtists = make(map[string]string)
	x.listEvents = make(map[string]s.EventsStruct)
	x.airport = ai
	return x
}

func getResponse(address string) []byte {
	address = strings.Replace(address, " ", "%20", -1)
	log.Println("URL LF ", address)
	resp, err := http.Get(address)
	if err != nil {
		log.Println("ERR ", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("ERR ", err)
	}
	return body
}

func buildUrl(method, params string) string {
	url := urlBase + `method=` + method + `&` + params
	log.Println(url)
	return url
}
