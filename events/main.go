package main

import (
	"encoding/json"
	a "events/airports"
	lf "events/lastfm"
	sg "events/seatgeek"
	s "events/struct"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
)

var lfi *lf.LastFmInstance
var sgi *sg.SeatgeekInstance
var ai *a.AirportInstance

func main() {
	log.Println("start")
	ai = a.Init()
	lfi = lf.Init(ai)
	sgi = sg.Init(ai)

	http.HandleFunc("/artists", reqArtistsHandler)
	http.HandleFunc("/events", reqEventsHandler)
	http.HandleFunc("/event", reqEventHandler)
	http.HandleFunc("/user", reqUserHandler)
	http.HandleFunc("/reservation", reqReservationHandler)
	http.ListenAndServe(":8080", nil)
}

func reqReservationHandler(rw http.ResponseWriter, req *http.Request) {
	name := req.FormValue("id")
	if name == "" {
		rw.Write([]byte(`{"msg":"id is mandatory"}`))
		return
	}
	rand.Seed(time.Now().UTC().UnixNano())

	booknr := randomchar(6)
	rw.Write([]byte(`{"price":0,"booknr":"` + booknr + `"}`))
}

func reqUserHandler(rw http.ResponseWriter, req *http.Request) {
	name := req.FormValue("id")
	if name == "" {
		rw.Write([]byte(`{"msg":"id is mandatory"}`))
		return
	}
	list := lfi.GetUserArtists(name)

	resp, _ := json.Marshal(list)
	rw.Write(resp)
}

func reqArtistsHandler(rw http.ResponseWriter, req *http.Request) {
	artist := req.FormValue("q")
	if artist == "" {
		rw.Write([]byte(`{"msg":"q is mandatory"}`))
		return
	}
	var list s.ResponseArtists
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		a := lfi.SearchArtists(artist)
		list.Artists = append(list.Artists, a.Artists...)
		wg.Done()
	}()
	go func() {
		a := sgi.SearchArtists(artist)
		list.Artists = append(list.Artists, a.Artists...)
		wg.Done()
	}()
	wg.Wait()
	resp, _ := json.Marshal(list)
	rw.Write(resp)
}

func reqEventsHandler(rw http.ResponseWriter, req *http.Request) {
	idevents := req.FormValue("id")
	if idevents == "" {
		rw.Write([]byte(`{"msg":"id is mandatory"}`))
		return
	}
	idlist := strings.Split(idevents, ",")
	rw.Write(searchEvents(idlist))
}

func reqEventHandler(rw http.ResponseWriter, req *http.Request) {
	idevent := req.FormValue("id")
	artistid := req.FormValue("artistid")
	userlat := req.FormValue("lat")
	userlng := req.FormValue("lng")
	if idevent == "" {
		rw.Write([]byte(`{"msg":"id is mandatory"}`))
		return
	}
	rw.Write(searchEvent(idevent, artistid, userlat, userlng))
}

func searchEvent(id string, artistid string, userlat, userlng string) []byte {
	if len(id) > 2 {
		if id[0:2] == "LF" {
			event := lfi.EventInfo(id[2:], artistid, userlat, userlng)
			resp, _ := json.Marshal(event)
			return resp
		} else if id[0:2] == "SG" {
			event := sgi.EventInfo(id[2:], artistid, userlat, userlng)
			resp, _ := json.Marshal(event)
			return resp
		}
	}
	return []byte("")
}

func searchEvents(id []string) []byte {
	list := s.ResponseEvents{}
	readCh := make(chan []s.EventsStruct, 100)
	wg := sync.WaitGroup{}
	go func() {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered in f searchevent 1", r)
			}
		}()
		for {
			x := <-readCh
			list.Events = append(list.Events, x...)
			wg.Done()
		}
	}()

	for _, a := range id {
		if len(a) > 2 {
			artist := a[2:]
			if a[0:2] == "LF" {
				wg.Add(1)
				go func(ch chan []s.EventsStruct, idartist string) {
					defer func() {
						if r := recover(); r != nil {
							fmt.Println("Recovered in f searchevent 2", r)
							wg.Done()
						}
					}()
					ch <- lfi.SearchEvents(idartist)
				}(readCh, artist)
			} else if a[0:2] == "SG" {
				wg.Add(1)
				go func(ch chan []s.EventsStruct, idartist string) {
					defer func() {
						if r := recover(); r != nil {
							fmt.Println("Recovered in f searchevent 2", r)
							wg.Done()
						}
					}()
					ch <- sgi.SearchEvents(idartist)
				}(readCh, artist)
			}
		}
	}
	wg.Wait()

	// TODO sortowanie po dacie
	resp, _ := json.Marshal(list)
	return resp
}

var alpha = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"

// generates a random string of fixed size
func randomchar(size int) string {
	buf := make([]byte, size)
	for i := 0; i < size; i++ {
		buf[i] = alpha[rand.Intn(len(alpha))]
	}
	return string(buf)
}
