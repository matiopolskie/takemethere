package structResponse

import (
	"time"
)

type ResponseArtists struct {
	Artists []ArtistData `json:"artists"`
}

type ArtistData struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type ResponseEvents struct {
	Events []EventsStruct `json:"events"`
}

type EventsStruct struct {
	Id               string    `json:"id"`
	Title            string    `json:"title"`
	Startdate        string    `json:"startdate"`
	StartdateTime    time.Time `json:"-"`
	Enddate          string    `json:"enddate"`
	Img              string    `json:"img"`
	Lat              string    `json:"lat"`
	Lng              string    `json:"lng"`
	City             string    `json:"city"`
	Address          string    `json:"address"`
	Price            float64   `json:"price"`
	Performers       string    `json:"performers"`
	Description      string    `json:"desc"`
	ArtistSearchName string    `json:"artistSearchName"`
	ArtistSearchId   string    `json:"artistSearchId"`
	DepartureCode    string    `json:"departureCode"`
	DepartureDesc    string    `json:"departureDesc"`
	DestinationCode  string    `json:"destinationCode"`
	DestinationDesc  string    `json:"destinationDesc"`
}
