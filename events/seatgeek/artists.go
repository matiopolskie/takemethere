package seatgeek

import (
	"encoding/json"
	s "events/struct"
	"log"
	"strconv"
)

func (sgi *SeatgeekInstance) SearchArtists(artist string) s.ResponseArtists {
	x := getResponse(urlBasePerformers + artist)
	return sgi.parseArtists(x)
}

func (sgi *SeatgeekInstance) parseArtists(resp []byte) s.ResponseArtists {
	respParse := SGPerformers{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}
	x := s.ResponseArtists{}
	for _, a := range respParse.Performers {
		if a.Id > 0 {
			sgi.listArtists[a.Id] = a.Name
			y := s.ArtistData{}
			y.Id = "SG" + strconv.Itoa(a.Id)
			y.Name = a.Name
			x.Artists = append(x.Artists, y)
		}
	}
	return x
}
