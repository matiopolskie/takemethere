package seatgeek

import (
	a "events/airports"
	s "events/struct"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var urlBasePerformers string = `http://api.seatgeek.com/2/performers?q=`
var urlBaseEvents string = `http://api.seatgeek.com/2/events?`
var urlBaseEvent string = `http://api.seatgeek.com/2/events/`

type SeatgeekInstance struct {
	listArtists map[int]string
	airport     *a.AirportInstance
	listEvents  map[string]s.EventsStruct
}

func Init(ai *a.AirportInstance) *SeatgeekInstance {
	x := &SeatgeekInstance{}
	x.listArtists = make(map[int]string)
	x.listEvents = make(map[string]s.EventsStruct)
	x.airport = ai
	return x
}

func getResponse(address string) []byte {
	log.Println("URL SG ", address)
	address = strings.Replace(address, " ", "+", -1)
	resp, err := http.Get(address)
	if err != nil {
		log.Println("ERR SG getResponse 1 ", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("ERR SG getResponse 2 ", err)
	}
	return body
}
