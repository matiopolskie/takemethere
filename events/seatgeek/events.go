package seatgeek

import (
	"encoding/json"
	s "events/struct"
	"log"
	"strconv"
	"strings"
	"time"
)

func (sgi *SeatgeekInstance) SearchEvents(id string) []s.EventsStruct {
	url := urlBaseEvents + "performers.id=" + id
	x := getResponse(url)
	return sgi.parseEvents(x, id)
}

func (sgi *SeatgeekInstance) parseEvents(resp []byte, id string) []s.EventsStruct {
	respParse := SGEvents{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}
	log.Println(respParse)
	var x []s.EventsStruct

	for _, a := range respParse.Events {
		htl, b := sgi.parseEventInfo(a, id, "", "", false)
		if b {
			x = append(x, htl)
		}
	}
	return x
}
func (sgi *SeatgeekInstance) parseEventInfo(r SGEvent, id string, userlat, userlng string, extendinfo bool) (s.EventsStruct, bool) {
	y := s.EventsStruct{}
	if r.Venue.Location.Lat == 0 && r.Venue.Location.Lng == 0 {
		return y, false
	}

	y.Id = "SG" + strconv.Itoa(r.Id)
	y.Title = r.Title
	if r.Startdate != "" {
		t1, err := time.Parse("2006-01-02T15:04:05", r.Startdate)
		if err != nil {
			log.Println("ERR Events 2 DATE ", err)
		} else {
			y.StartdateTime = t1
			y.Startdate = t1.Format("2006-01-02 15:04:05")
		}
	}
	// y.Img = findImage(r.Img)

	y.Lat = strconv.FormatFloat(r.Venue.Location.Lat, 'f', 10, 64)
	y.Lng = strconv.FormatFloat(r.Venue.Location.Lng, 'f', 10, 64)

	lat := r.Venue.Location.Lat
	lng := r.Venue.Location.Lng
	y.DestinationCode, y.DestinationDesc = sgi.airport.GetNearAirport(lat, lng)

	if userlat != "" && userlng != "" {
		lat, err1 := strconv.ParseFloat(userlat, 64)
		lng, err2 := strconv.ParseFloat(userlng, 64)
		if err1 != nil || err2 != nil {
			log.Println("GPS LF LNG ", err1, err2)
		} else {
			y.DepartureCode, y.DepartureDesc = sgi.airport.GetNearAirport(lat, lng)
		}
	}

	y.City = r.Venue.City
	var listPerformers []string
	for _, x := range r.Performers {
		listPerformers = append(listPerformers, x.Name)
	}
	y.Performers = strings.Join(listPerformers, ", ")
	id = strings.Replace(id, "SG", "", -1)

	log.Println("ID ", id)
	log.Println("sg ", sgi.listArtists)
	idint, err := strconv.Atoi(id)
	if err != nil {
		log.Println("ERR ", idint)
	} else {
		y.ArtistSearchName = sgi.listArtists[idint]
		y.ArtistSearchId = "SG" + id
	}
	y.Price = r.Stats.AveragePrice
	return y, true
}
