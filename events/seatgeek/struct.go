package seatgeek

type SGPerformers struct {
	Performers []SGPerformer `json:"performers"`
}

type SGPerformer struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type SGEvents struct {
	Events []SGEvent `json:"events"`
}

type SGEvent struct {
	Id    int `json:"id"`
	Stats struct {
		AveragePrice float64 `json:"average_price"`
	} `json:"stats"`
	Title      string        `json:"title"`
	Startdate  string        `json:"datetime_local"`
	Venue      SGVenue       `json:"venue"`
	Performers []SGPerformer `json:"performers"`
}

type SGVenue struct {
	City     string     `json:"city"`
	Location SGLocation `json:"location"`
}

type SGLocation struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lon"`
}
