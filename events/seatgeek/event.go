package seatgeek

import (
	"encoding/json"
	s "events/struct"
	"log"
)

func (sgi *SeatgeekInstance) EventInfo(id, artistid string, userlat, userlng string) s.EventsStruct {
	url := urlBaseEvent + id
	log.Println(url)
	resp := getResponse(url)
	respParse := SGEvent{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}
	htl, _ := sgi.parseEventInfo(respParse, artistid, userlat, userlng, true)
	sgi.listEvents[htl.Id] = htl
	log.Println("LIST", sgi.listEvents)
	return htl
}
