package airports

import (
	"encoding/csv"
	"fmt"
	geo "github.com/kellydunn/golang-geo"
	"log"
	"os"
	"strconv"
)

type AirportInstance struct {
	listGeo      []*geo.Point
	listAirports []Airport
}

type Airport struct {
	Code string
	Desc string
}

func Init() *AirportInstance {
	x := &AirportInstance{}
	x.getPoints()

	return x
}

func (a *AirportInstance) getPoints() {
	csvfile, err := os.Open("airports.csv")
	if err != nil {
		fmt.Println(err)
		return
	}

	defer csvfile.Close()
	reader := csv.NewReader(csvfile)

	reader.FieldsPerRecord = -1 // see the Reader struct information below

	rawCSVdata, err := reader.ReadAll()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var tmp []Airport
	var tmp2 []*geo.Point
mainloop:
	for _, x := range rawCSVdata {
		lat, err := strconv.ParseFloat(x[4], 64)
		if err != nil {
			log.Println(err)
			continue mainloop
		}
		lng, err := strconv.ParseFloat(x[5], 64)
		if err != nil {
			log.Println(err)
			continue mainloop
		}
		gps := geo.NewPoint(lat, lng)
		tmp = append(tmp, Airport{Code: x[0], Desc: x[1]})
		tmp2 = append(tmp2, gps)
	}
	a.listAirports = tmp
	a.listGeo = tmp2
}

func (a *AirportInstance) GetNearAirport(lat, lng float64) (string, string) {
	g1 := geo.NewPoint(lat, lng)
	var oldDist float64 = 10000
	id := 0
	for i, g := range a.listGeo {
		dist := g.GreatCircleDistance(g1)
		if oldDist > dist {
			oldDist = dist
			id = i
		}
	}
	return a.listAirports[id].Code, a.listAirports[id].Desc
}
