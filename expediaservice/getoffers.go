package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
)

func getOffers(lat, lng string, latfloat, lngfloat float64, adt string, startdate, enddate string) ResponseOffers {
	xml := `<HotelListRequest>
    <latitude>` + lat + `</latitude>
    <longitude>` + lng + `</longitude>
    <searchRadius>20</searchRadius>
    <arrivalDate>` + startdate + `</arrivalDate>
    <departureDate>` + enddate + `</departureDate>
    <RoomGroup>
        <Room>
            <numberOfAdults>` + adt + `</numberOfAdults>
        </Room>
    </RoomGroup>
	<sort>PRICE_AVERAGE</sort>
    <numberOfResults>25</numberOfResults>
</HotelListRequest>`
	log.Println(xml)
	resp := getResponse(xml)
	respParse := OffersJson{}
	if err := json.Unmarshal(resp, &respParse); err != nil {
		log.Println("ERR ", err)
	}
	x := ResponseOffers{}

	for _, h := range respParse.HotelListResponse.HotelList.HotelSummary {

		img := ""
		if h.ThumbNailUrl != "" {
			img = addressMedia + h.ThumbNailUrl
		}
		var postalcode string
		switch h.Postalcode.(type) {
		case string:
			postalcode = h.Postalcode.(string)
		case int:
			postalcode = strconv.Itoa(h.Postalcode.(int))
		}
		startdate1 := ""
		enddate1 := ""
		if len(startdate) == 10 {
			startdate1 = startdate[6:10] + "-" + startdate[0:2] + "-" + startdate[3:5]
		}
		if len(enddate) == 10 {
			enddate1 = enddate[6:10] + "-" + enddate[0:2] + "-" + enddate[3:5]
		}
		htl := ResponseHotel{
			Id:               h.Id,
			Name:             h.Name,
			Address:          h.Address,
			City:             h.City,
			Postalcode:       postalcode,
			CountryCode:      h.CountryCode,
			Latitude:         h.Latitude,
			Longitude:        h.Longitude,
			ShortDescription: h.ShortDescription,
			Price:            h.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.ChargeableRateInfo.Total,
			Currency:         "USD",
			Distance:         strconv.FormatFloat(h.ProximityDistance, 'f', 2, 64),
			DistanceUnit:     h.ProximityUnit,
			Img:              img,
			Rating:           h.HotelRating,
			StartDate:        startdate1,
			EndDate:          enddate1}
		hash := generateHash(htl)
		htl.Hash = hash
		x.Hotels = append(x.Hotels, htl)

		log.Println("HASH ", hash)
		listHotels[hash] = htl
	}

	return x
}

func generateHash(r ResponseHotel) string {
	oPart, _ := json.Marshal(r)
	return fmt.Sprintf("%x", md5.Sum(oPart))
}
