package main

type ResponseOffers struct {
	Hotels []ResponseHotel `json:"hotels"`
}

type ResponseHotel struct {
	Id               int     `json:"Id"`
	Hash             string  `json:"hash"`
	Name             string  `json:"name"`
	Address          string  `json:"address"`
	City             string  `json:"city"`
	Postalcode       string  `json:"postalcode"`
	CountryCode      string  `json:"countrycode"`
	Latitude         float64 `json:"latitude"`
	Longitude        float64 `json:"longitude"`
	ShortDescription string  `json:"shortDescription"`
	Price            string  `json:"total"`
	Currency         string  `json:"currency"`
	Distance         string  `json:"distance"`
	DistanceUnit     string  `json:"distanceUnit"`
	Img              string  `json:"img"`
	Rating           float64 `json:"rating"`
	StartDate        string  `json:"startdate"`
	EndDate          string  `json:"enddate"`
}
