package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

var address string = `http://api.ean.com/ean-services/rs/hotel/v3/list?cid=XXXXX&minorRev=99&apiKey=XXXXXXXXXXXXXX&locale=en_US&currencyCode=USD&xml=`
var addressGiata string = `http://multicodes.giatamedia.com/webservice/rest/1.0/properties/tourOperator/EXPE/`
var addressMedia string = `http://media.expedia.com/`
var listHotels map[string]ResponseHotel

func main() {
	listHotels = make(map[string]ResponseHotel)
	log.Println("START")
	http.HandleFunc("/offers", reqGetOffersHandler)
	http.HandleFunc("/offer", reqGetOfferHandler)
	http.HandleFunc("/gethash", reqGetIdHandler)
	http.ListenAndServe(":8888", nil)
}
func reqGetIdHandler(rw http.ResponseWriter, req *http.Request) {
	hash := req.FormValue("hash")
	if hash == "" {
		rw.Write([]byte(`{"msg":"hash is mandatory"}`))
		return
	}
	resp, _ := json.Marshal(GetId(hash))
	rw.Write(resp)
}

func reqGetOfferHandler(rw http.ResponseWriter, req *http.Request) {
	code := req.FormValue("code")
	lat := req.FormValue("pointlat")
	lng := req.FormValue("pointlng")

	x := getOffer(lat, lng, code)
	resp, _ := json.Marshal(x)
	rw.Write(resp)
}

func reqGetOffersHandler(rw http.ResponseWriter, req *http.Request) {
	lat := req.FormValue("lat")
	lng := req.FormValue("lng")
	adt := req.FormValue("adt")
	startdate := req.FormValue("startdate")
	enddate := req.FormValue("enddate")

	if lat == "" || lng == "" || adt == "" || startdate == "" || enddate == "" {
		rw.Write([]byte(`{"msg":"lat, lng, startdate, enddate and adt is mandatory"}`))
		return
	}
	// startdate = 7 / 27 / 2015
	s1, err1 := time.Parse("2006-01-02", startdate)
	s2, err2 := time.Parse("2006-01-02", enddate)
	if err1 != nil || err2 != nil {
		rw.Write([]byte(`{"msg":"startdate and enddate must be format YYYY-MM-DD"}`))
		return
	}

	latfloat, err1 := strconv.ParseFloat(lat, 64)
	lngfloat, err2 := strconv.ParseFloat(lng, 64)

	if err1 != nil || err2 != nil {
		rw.Write([]byte(`{"msg":"lat or lng not float64"}`))
		return
	}

	x := getOffers(lat, lng, latfloat, lngfloat, adt, s1.Format("01/02/2006"), s2.Format("01/02/2006"))
	resp, _ := json.Marshal(x)
	rw.Write(resp)
}

func getResponse(xml string) []byte {
	urladdress := address + url.QueryEscape(xml)
	resp, err := http.Get(urladdress)
	if err != nil {
		log.Println("ERR getResponse 1 ", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("ERR getResponse 2 ", err)
	}

	return body
}
