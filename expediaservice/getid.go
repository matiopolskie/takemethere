package main

func GetId(hash string) interface{} {
	if listHotels[hash].Id > 0 {
		x := ResponseOffers{}
		x.Hotels = append(x.Hotels, listHotels[hash])
		return x
	}
	return nil
}
