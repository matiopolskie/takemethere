package main

import (
	"io/ioutil"
	"log"
	"net/http"
)

func getOffer(lat, lng, code string) ResponseOffers {
	x := ResponseOffers{}
	// x.Hotels = append(x.Hotels, )
	y := getGiataResponse(code)
	log.Println(string(y))
	return x
}

func getGiataResponse(code string) []byte {
	urladdress := addressGiata + code
	client := &http.Client{}
	req, err := http.NewRequest("GET", urladdress, nil)
	req.Header.Add("Authorization", `Basic XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"`)
	resp, err := client.Do(req)

	// resp, err := http.Get(urladdress)
	if err != nil {
		log.Println("ERR getResponse 1 ", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("ERR getResponse 2 ", err)
	}
	log.Println(string(body))
	return body
}
