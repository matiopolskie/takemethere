package main

type OffersJson struct {
	HotelListResponse HotelListResponse `json:"HotelListResponse"`
}

type HotelListResponse struct {
	HotelList HotelList `json:"HotelList"`
}

type HotelList struct {
	HotelSummary []HotelSummary `json:"HotelSummary"`
}

type HotelSummary struct {
	Id                  int                 `json:"hotelId"`
	Name                string              `json:"name"`
	Address             string              `json:"address1"`
	City                string              `json:"city"`
	Postalcode          interface{}         `json:"postalCode"`
	CountryCode         string              `json:"countryCode"`
	Latitude            float64             `json:"latitude"`
	Longitude           float64             `json:"longitude"`
	ShortDescription    string              `json:"shortDescription"`
	RoomRateDetailsList RoomRateDetailsList `json:"RoomRateDetailsList"`
	ProximityDistance   float64             `json:"proximityDistance"`
	ProximityUnit       string              `json:"proximityUnit"`
	ThumbNailUrl        string              `json:"thumbNailUrl"`
	HotelRating         float64             `json:"hotelRating"`
}

type RoomRateDetailsList struct {
	RoomRateDetails RoomRateDetails `json:"RoomRateDetails"`
}
type RoomRateDetails struct {
	RateInfos RateInfos `json:"RateInfos"`
}
type RateInfos struct {
	RateInfo struct {
		ChargeableRateInfo struct {
			Total string `json:"@total"`
		} `json:"ChargeableRateInfo"`
	} `json:"RateInfo"`
}
